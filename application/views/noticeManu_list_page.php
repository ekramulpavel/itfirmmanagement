<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
		<title>IT Farm Management</title>
		<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="header_wrapper">
			<img src="<?php echo base_url('assets/images/banner.png'); ?>"/>
		</div>

		<div class="main_wrapper">

		<div class="menubar">
			<ul id="menu">
				<li><a href="<?php echo site_url('site/home_list_page');?>">Home</a></li>
				<li><a href="<?php echo site_url('site/allProduct_list_page');?>">All Products</a></li>
				<li><a href="<?php echo site_url('site/noticeManu_list_page');?>">Notice Board</a></li>
				<li><a href="<?php echo site_url('site/login_list_page');?>">Log in</a></li>
				<li><a href="<?php echo site_url('site/about_list_page');?>">About Us</a></li>
				<li><a href="<?php echo site_url('site/contact_list_page');?>">Contact Us</a></li>

			</ul>
			<div id="form">
				<form method="get" action="results.php" enctype ="multipart/form-data">
					<input type="text" name="user_query" placeholder="Search a product"/>
					<input type="submit" name="search" value="Search"/>
				</form>
			</div>
		</div>
		<div class="contain_wrapper">

			<div class="main_content">
				<div id="table_decoration">
					<h1 align="center" style=";background: skyblue;color:white;margin: 5px 300px 5px 300px">Notice Board</h1>

			<table >
			<thead style="background: skyblue;">
				<th>Notice Type</th>
				<th>Date Of Notice Publish</th>
				<th>Description</th>
			</thead>
			<tbody>
			<?php
			foreach ($notices as $notice) {
			?>

				<tr>
					<td><?php echo $notice['n_id'];?></td>
					<td><?php echo $notice['date'];?></td>
					<td><?php echo $notice['description'];?></td>

				</tr>


			<?php
			}
			?>
			</tbody>
			</table>

				</div>
			</div>
			</div>

		</div>
		<div id="footer">
			<h2>&copy; NSU</h2>
		</div>

	</body>



</html>
