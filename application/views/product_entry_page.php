<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
		<title>IT Firm Management</title>
		<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="header_wrapper">
			<img src="<?php echo base_url('assets/images/banner.png'); ?>"/>
		</div>

		<div class="main_wrapper">

		<div class="menubar">
			<ul id="menu">
				<li><a href="<?php echo site_url('site/home_list_page');?>">Home</a></li>
				<li><a href="<?php echo site_url('site/allProduct_list_page');?>">All Products</a></li>
				<li><a href="<?php echo site_url('site/noticeManu_list_page');?>">Notice Board</a></li>
				<li><a href="<?php echo site_url('site/login_list_page');?>">Log in</a></li>
				<li><a href="<?php echo site_url('site/about_list_page');?>">About Us</a></li>
				<li><a href="<?php echo site_url('site/contact_list_page');?>">Contact Us</a></li>

			</ul>
			<div id="form">
				<form method="get" action="results.php" enctype ="multipart/form-data">
					<input type="text" name="user_query" placeholder="Search a product"/>
					<input type="submit" name="search" value="Search"/>
				</form>
			</div>
		</div>
		<div class="contain_wrapper">

			<div class="main_content">

					<h1 align="center" style=";background: skyblue;color:white;margin: 25px 249px 5px 249px">Product Entry</h1>



		<div id="form_decoration">
		<form name="employeeInsert" method="post" action="<?php echo site_url('site/product_entry');?>">
				<table>
					<tr>
						<td align="right"><b>Category:</b></td>
						<td style="padding:5px;">
							<select name="category_id" style="width:173px">
							<option value="">Select Category</option>
							<?php
							foreach ($categories as $category) {
							?>

							<option value="<?php echo $category['cat_id'];?>"><?php echo $category['type'];?></option>
				<?php
				}

			?>
			</select>

						</td>
					</tr>
					<tr>
						<td align="right"><b>Name:</b></td>
						<td style="padding:5px;"><input type="text" name="name" required="required"></td>
					</tr>
					<tr>
						<td align="right"><b>Date Of publish:</b></td>
						<td style="padding:5px;"><input type="text" name="date_publish" required="required"></td>
					</tr>
					<tr>
						<td align="right"><b>Description:</b></td>
						<td style="padding:5px;"><input type="text" name="description" required="required"></td>
					</tr>
					<tr>
						<td align="right"><b>Price:</b></td>
						<td style="padding:5px;"><input type="text" name="price" required="required"></td>
					</tr>

					<tr align="center">
						<td colspan="8"><input type="submit" value="Save" name="submit" /></td>
					</tr>

				</table>

		</form>

		</div>
			</div>
			</div>

		</div>
		<div id="footer">
			<h2>&copy; NSU</h2>
		</div>

	</body>



</html>
