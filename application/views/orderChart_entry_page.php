<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
		<title>IT Firm Management</title>
		<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="header_wrapper">
			<img src="<?php echo base_url('assets/images/banner.png'); ?>"/>
		</div>

		<div class="main_wrapper">

		<div class="menubar">
			<ul id="menu">
				<li><a href="<?php echo site_url('site/home_list_page');?>">Home</a></li>
				<li><a href="<?php echo site_url('site/allProduct_list_page');?>">All Products</a></li>
				<li><a href="<?php echo site_url('site/noticeManu_list_page');?>">Notice Board</a></li>
				<li><a href="<?php echo site_url('site/login_list_page');?>">Log in</a></li>
				<li><a href="<?php echo site_url('site/about_list_page');?>">About Us</a></li>
				<li><a href="<?php echo site_url('site/contact_list_page');?>">Contact Us</a></li>

			</ul>
			<div id="form">
				<form method="get" action="results.php" enctype ="multipart/form-data">
					<input type="text" name="user_query" placeholder="Search a product"/>
					<input type="submit" name="search" value="Search"/>
				</form>
			</div>
		</div>
		<div class="contain_wrapper">

			<div class="main_content">

					<h1 align="center" style=";background: skyblue;color:white;margin: 25px 249px 5px 249px">OrderChart Entry</h1>



		<div id="form_decoration">
		<form name="customerInsert" method="post" action="<?php echo site_url('site/orderChart_entry');?>">
				<table>
					<tr>
						<td align="right"><b>Customer Id:</b></td>
						<td style="padding:5px;">
							<select name="customer_id" style="width:173px">
							<option value="">Select Customer Id</option>
							<?php
							foreach ($customers as $customer) {
							?>

							<option value="<?php echo $customer['c_id'];?>"><?php echo $customer['c_id'];?></option>
				<?php
				}

			?>
			</select>

						</td>
					</tr>
					<tr>
						<td align="right"><b>Product Name:</b></td>
						<td style="padding:5px;">
							<select name="product_id" style="width:173px">
							<option value="">Select Product</option>
							<?php
							foreach ($products as $product) {
							?>

							<option value="<?php echo $product['p_id'];?>"><?php echo $product['name'];?></option>
				<?php
				}

			?>
			</select>

						</td>
					</tr>
					<tr>
						<td align="right"><b>Date:</b></td>
						<td style="padding:5px;"><input type="text" name="date" required="required"></td>
					</tr>


					<tr align="center">
						<td colspan="8"><input type="submit" value="Save" name="submit" /></td>
					</tr>

				</table>

		</form>

		</div>
			</div>
			</div>

		</div>
		<div id="footer">
			<h2>&copy;NSU</h2>
		</div>

	</body>



</html>
