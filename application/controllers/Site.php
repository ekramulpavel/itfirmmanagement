<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

 	public function __construct()
    {
        parent::__construct();
        $this->load->model('SiteModelView');
        $this->load->model('SiteModelInsert');
    }
	public function index()
	{
		$this->data['products']=$this->SiteModelView->getAllProductsForHome();

		$this->load->view('home_list_page',$this->data);
		
	}








	/* My code start from here*/
	public function employee_list_page(){
		$this->data['employees']=$this->SiteModelView->getAllEmployees();

		$this->load->view('employee_list_page',$this->data);
	}
	public function employee_entry_page(){
		$this->load->view('employee_entry_page');
	}
	public function employee_entry(){
		if($this->SiteModelInsert->employeeInsert())
			redirect('/site/employee_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function customer_list_page(){
		$this->data['customers']=$this->SiteModelView->getAllCustomers();

		$this->load->view('customer_list_page',$this->data);
	}
	public function customer_entry_page(){
		$this->load->view('customer_entry_page');
	}
	public function customer_entry(){
		if($this->SiteModelInsert->customerInsert())
			redirect('/site/customer_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function noticeboard_list_page(){
		$this->data['notices']=$this->SiteModelView->getAllNotices();

		$this->load->view('noticeboard_list_page',$this->data);
	}
	public function noticeboard_entry_page(){
		$this->load->view('noticeboard_entry_page');
	}
	public function noticeboard_entry(){
		if($this->SiteModelInsert->noticeInsert())
			redirect('/site/noticeboard_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function category_list_page(){
		$this->data['categories']=$this->SiteModelView->getAllCategories();

		$this->load->view('category_list_page',$this->data);
	}
	public function category_entry_page(){
		$this->load->view('category_entry_page');
	}
	public function category_entry(){
		if($this->SiteModelInsert->categoryInsert())
			redirect('/site/category_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function expenseType_list_page(){
		$this->data['enpenses_type']=$this->SiteModelView->getAllexpenseTypes();

		$this->load->view('expenseType_list_page',$this->data);
	}
	public function expenseType_entry_page(){
		$this->load->view('expenseType_entry_page');
	}
	public function expenseType_entry(){
		if($this->SiteModelInsert->expenseTypeInsert())
			redirect('/site/expenseType_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}

	public function expense_list_page(){
		$this->data['expenses']=$this->SiteModelView->getAllexpenses();

		$this->load->view('expense_list_page',$this->data);
	}
	public function expense_entry_page(){
		$this->data['expenses_type']=$this->SiteModelView->getAllexpenseTypes();
		$this->load->view('expense_entry_page',$this->data);
	}
	public function expense_entry(){
		if($this->SiteModelInsert->expenseInsert())
			redirect('/site/expense_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function product_list_page(){
		$this->data['products']=$this->SiteModelView->getAllProducts();

		$this->load->view('product_list_page',$this->data);
	}
	public function product_entry_page(){
		$this->data['categories']=$this->SiteModelView->getAllCategories();
		$this->load->view('product_entry_page',$this->data);
	}
	public function product_entry(){
		if($this->SiteModelInsert->productInsert())
			redirect('/site/product_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function order_list_page(){
		$this->data['orders']=$this->SiteModelView->getAllOrders();

		$this->load->view('order_list_page',$this->data);
	}
	public function order_entry_page(){
		$this->data['customers']=$this->SiteModelView->getAllCustomers();
		$this->load->view('order_entry_page',$this->data);
	}
	public function order_entry(){
		if($this->SiteModelInsert->orderChartInsert())
			redirect('/site/orderChart_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function orderChart_list_page(){
		$this->data['orderCharts']=$this->SiteModelView->getAllOrderCharts();

		$this->load->view('orderChart_list_page',$this->data);
	}
	public function orderChart_entry_page(){
		$this->data['customers']=$this->SiteModelView->getAllCustomers();
		$this->data['products']=$this->SiteModelView->getAllProducts();
		$this->load->view('orderChart_entry_page',$this->data);
	}
	public function orderChart_entry(){
		if($this->SiteModelInsert->orderChartInsert())
			redirect('/site/orderChart_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function profit_list_page(){
		$this->data['orderCharts']=$this->SiteModelView->getAllOrderCharts();
		$this->data['orders']=$this->SiteModelView->getAllOrders();
		$this->data['profitFromCharts']=$this->SiteModelView->getAllProfitFromCharts();
		$this->data['profitFromLists']=$this->SiteModelView->getAllProfitFromLists();

		$this->load->view('profit_list_page',$this->data);
	}
	
	public function salary_list_page(){
		$this->data['salaries']=$this->SiteModelView->getAllSalaries();

		$this->load->view('salary_list_page',$this->data);
	}
	public function salary_entry_page(){
		$this->data['employees']=$this->SiteModelView->getAllEmployees();
		$this->load->view('salary_entry_page',$this->data);
	}
	public function salary_entry(){
		if($this->SiteModelInsert->salaryInsert())
			redirect('/site/salary_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function salaryChart_list_page(){
		$this->data['salaryCharts']=$this->SiteModelView->getAllSalaryCharts();

		$this->load->view('salaryChart_list_page',$this->data);
	}
	public function salaryChart_entry_page(){
		$this->data['employees']=$this->SiteModelView->getAllEmployees();
		$this->load->view('salaryChart_entry_page',$this->data);
	}
	public function salaryChart_entry(){
		if($this->SiteModelInsert->salaryChartInsert())
			redirect('/site/salaryChart_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}

	public function user_list_page(){
		$this->data['users']=$this->SiteModelView->getAllUsers();

		$this->load->view('user_list_page',$this->data);
	}
	public function user_entry_page(){
		$this->load->view('user_entry_page');
	}
	public function user_entry(){
		if($this->SiteModelInsert->userInsert())
			redirect('/site/user_list_page');
		else
			echo "Ooopse! Could no do it :-(";
	}
	public function login_list_page(){

		$this->load->view('login_list_page');
	}
	public function adminAccess_list_page(){

		$this->load->view('adminAccess_list_page');
	}
	public function employeeAccess_list_page(){

		$this->load->view('employeeAccess_list_page');
	}

	public function accountAccess_list_page(){

		$this->load->view('accountAccess_list_page');
	}
	public function customerAccess_list_page(){

		$this->load->view('customerAccess_list_page');
	}

	public function about_list_page(){

		$this->load->view('about_list_page');
	}
	public function contact_list_page(){

		$this->load->view('contact_list_page');
	}
	public function noticeManu_list_page(){

		$this->data['notices']=$this->SiteModelView->getAllNotices();

		$this->load->view('noticeManu_list_page',$this->data);
	}
	public function home_list_page(){
		$this->data['products']=$this->SiteModelView->getAllProductsForHome();

		$this->load->view('home_list_page',$this->data);
	}
	public function allProduct_list_page(){
		$this->data['products']=$this->SiteModelView->getAllProducts();

		$this->load->view('allProduct_list_page',$this->data);
	}




























	/*end in here*/

	public function enroll_new()
	{
		$this->data['students']=$this->SiteModel->getAllStudents();
		$this->data['instructors']=$this->SiteModel->getAllInstructors();

		$this->data['enrolled_students']=$this->SiteModel->getAllEnrolledMembers();


		$this->load->view('enroll_student_form',$this->data);
	}
}
