<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
		<title>IT Farm Management</title>
		<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="header_wrapper">
			<img src="<?php echo base_url('assets/images/banner.png'); ?>"/>
		</div>

		<div class="main_wrapper">

		<div class="menubar">
			<ul id="menu">
				<li><a href="<?php echo site_url('site/home_list_page');?>">Home</a></li>
				<li><a href="<?php echo site_url('site/allProduct_list_page');?>">All Products</a></li>
				<li><a href="<?php echo site_url('site/noticeManu_list_page');?>">Notice Board</a></li>
				<li><a href="<?php echo site_url('site/login_list_page');?>">Log in</a></li>
				<li><a href="<?php echo site_url('site/about_list_page');?>">About Us</a></li>
				<li><a href="<?php echo site_url('site/contact_list_page');?>">Contact Us</a></li>

			</ul>
			<div id="form">
				<form method="get" action="results.php" enctype ="multipart/form-data">
					<input type="text" name="user_query" placeholder="Search a product"/>
					<input type="submit" name="search" value="Search"/>
				</form>
			</div>
		</div>
		<div class="contain_wrapper">

			<div class="main_content">

					<h1 align="center" style=";background: skyblue;color:white;margin: 25px 249px 5px 249px">Contact IT Firm Management</h1>
					<div id="about" style="align:center;background:skyblue;overflow:hidden;padding:10px;
					margin:10px;border:1px solid yellow;">
						<div id="aboutImage" style="float:left; margin-right:10px;">

					<h1>Address:</h1>
 					<p>Plot 15, Block B<br/>
					Bashundhara, <br/>
					Dhaka 1229<br/>
					Bangladesh<br/>

					Email: itfirmmanagement@gmail.com<br/>
					Website: www.itfirmmanagement.com</p>

					<h1><br/> <br/> Location Map:</h1>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.0923586296394!2d90.42402771498259!3d23.815314584557413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64c103a8093%3A0xd660a4f50365294a!2sNorth+South+University!5e0!3m2!1sen!2sbd!4v1449502436870" width="950" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>



					</div>

					</div>




			</div>
		</div>

		</div>
		<div id="footer">
			<h2>&copy; NSU</h2>
		</div>

	</body>



</html>
