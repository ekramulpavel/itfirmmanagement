<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
		<title>IT Firm Management</title>
		<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="header_wrapper">
			<img src="<?php echo base_url('assets/images/banner.png'); ?>"/>
		</div>

		<div class="main_wrapper">

		<div class="menubar">
			<ul id="menu">
				<li><a href="<?php echo site_url('site/home_list_page');?>">Home</a></li>
				<li><a href="<?php echo site_url('site/allProduct_list_page');?>">All Products</a></li>
				<li><a href="<?php echo site_url('site/noticeManu_list_page');?>">Notice Board</a></li>
				<li><a href="<?php echo site_url('site/login_list_page');?>">Log in</a></li>
				<li><a href="<?php echo site_url('site/about_list_page');?>">About Us</a></li>
				<li><a href="<?php echo site_url('site/contact_list_page');?>">Contact Us</a></li>

			</ul>
			<div id="form">
				<form method="get" action="results.php" enctype ="multipart/form-data">
					<input type="text" name="user_query" placeholder="Search a product"/>
					<input type="submit" name="search" value="Search"/>
				</form>
			</div>
		</div>
		<div class="contain_wrapper1" style="width:1000px;
	    height:auto;
	    background:#ffffff;
	    border: 2px solid yellow;">


			<div class="main_content">

				<div id="about" style="align:center;background:skyblue;overflow:hidden;padding:10px;
					margin:10px;border:1px solid yellow;">
					<?php
					foreach ($products as $product) {
					?>
					<div id="pack" style="border:1px solid black;">
						<div id="aboutImage" style="float:left; margin-right:10px;">


					<img src="<?php echo base_url('assets/images/product/01.jpg'); ?>"/>


				</div>
				<div id="aboutDetails" style="padding:5px;">



					<b><h1> <?php echo $product['name'];?></h1>


					<p><strong>Category Type:</strong><?php echo $product['categoryType'];?></p>

					<p><strong>Date of Publish:</strong><?php echo $product['date_publish'];?></p>

					<p><strong>Price:</strong><?php echo $product['price'];?></p>

					<p><strong>Description:</strong><?php echo $product['description'];?></p>


		        </div>
		        <br/>
		        </div>
				<?php
				}
				?>
		    </div>
			</div>
			</div>

		</div>
		<div id="footer">
			<h2>&copy; NSU</h2>
		</div>

	</body>



</html>
