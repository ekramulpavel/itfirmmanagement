<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteModelView extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }
        public function getAllEmployees(){
                return  $this->db->get('employee')->result_array();
        }
        public function getAllCustomers(){
                return  $this->db->get('customer')->result_array();
        }
        public function getAllNotices(){
                return  $this->db->get('noticeboard')->result_array();
        }
        public function getAllCategories(){
                return  $this->db->get('category')->result_array();
        }
        public function getAllexpenseTypes(){
                return  $this->db->get('expense_type')->result_array();
        }
        public function getAllexpenses(){
                return $this->db->select('e.id as expenseId,ex.type as expenseType,
                        e.date as expenseDate,e.amount as expenseAmount')
                        ->from('expense AS e')
                        ->join('expense_type AS ex','e.ex_id=ex.id')
                        ->get()->result_array();
        }
        public function getAllProducts(){
                return $this->db->select('p.p_id,c.type as categoryType,p.name,p.date_publish,p.description,p.price')
                        ->from('product AS p')
                        ->join('category AS c','p.cat_id=c.cat_id')
                        ->get()->result_array();
        }
         public function getAllOrders(){
                return $this->db->select('o.id,c.c_id as customer_id,o.name ,o.description,o.order_date,o.delivery_date,o.price')
                        ->from('order_list AS o')
                        ->join('customer AS c','o.c_id=c.c_id')
                        ->get()->result_array();
        }
        public function getAllOrderCharts(){
                return $this->db->select('o.id,c.c_id as customerId,c.name as customerName,p.p_id as productId,p.description,
                        o.date,p.price')
                        ->from('order_chart AS o')
                        ->join('customer AS c','o.c_id=c.c_id')
                        ->join('product AS p','p.p_id=o.p_id')
                        ->get()->result_array();
        }
        public function getAllProfitFromCharts(){
                return $this->db->select('sum(p.price) as totalProfitFromChart')
                        ->from('order_chart AS o')
                        ->join('product AS p','o.p_id=p.p_id')
                        ->get()->result_array();
        }
        public function getAllProfitFromLists(){
                return $this->db->select('sum(o.price) as totalProfitFromList')
                        ->from('order_list AS o')
                        ->get()->result_array();
        }
        public function getAllSalaries(){
                return $this->db->select('s.id,e.e_id,e.name as employeeName,e.hire_date,s.salary')
                        ->from('salary AS s')
                        ->join('employee AS e','e.e_id=s.e_id')
                        ->get()->result_array();
        }
        public function getAllSalaryCharts(){
                return $this->db->select('sc.id,s.e_id,sc.date,s.salary,sc.payment,sc.withdraw,sc.bonus,sc.increment,sc.decrement')
                        ->from('salary AS s')
                        ->join('salary_chart AS sc','s.e_id=sc.e_id')
                        ->get()->result_array();
        }
        public function getAllUsers(){
                return  $this->db->get('users')->result_array();
        }
        public function getAllProductsForHome(){
                return $this->db->select('p.p_id,c.type as categoryType,p.name,p.date_publish,p.description,p.price')
                        ->from('product AS p')
                        ->join('category AS c','p.cat_id=c.cat_id')
                        ->limit(5)
                        ->get()->result_array();
        }

   
   

}