<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteModelInsert extends CI_Model {


        public $name;
        public $email;
        public $phone;
        public $dob;
        public $hire_date;
        public $registration_date;
        public $salary;
        public $address;
        public $image;
        public $type;
        public $date;
        public $description;
        public $ex_id;
        public $amount;
        public $cat_id;
        public $c_id;
        public $e_id;
        public $p_id;
        public $order_date;
        public $delivery_date;
        public $price;
        public $payment;
        public $bonus;
        public $increment;
        public $decrement;
        public $username;
        public $password;
        public $user_access;



        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();

        }

        public function employeeInsert()
        {
                 $this->name    = $this->input->post('name', TRUE);
                 $this->email     = $this->input->post('email', TRUE);
                 $this->phone  = $this->input->post('phone', TRUE);
                 $this->dob  = $this->input->post('dob', TRUE);
                 $this->hire_date  = $this->input->post('hire_date', TRUE);
                 $this->address  = $this->input->post('address', TRUE);

				$employee = array(
                'name' => $this->name,
                'email' =>  $this->email,
                'phone' => $this->phone,
                'dob' => $this->dob, 
                'hire_date' => $this->hire_date,
                'address' => $this->address,
                

                );
            	if($this->db->insert('employee', $employee))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function customerInsert()
        {
                 $this->name    = $this->input->post('name', TRUE);
                 $this->dob  = $this->input->post('dob', TRUE);
                 $this->email     = $this->input->post('email', TRUE);
                 $this->phone  = $this->input->post('phone', TRUE);
                 $this->address  = $this->input->post('address', TRUE);
                 $this->registration_date  = $this->input->post('registration_date', TRUE);
              
            	$customer = array(
                'name' => $this->name,
                'dob' => $this->dob, 
                'email' =>  $this->email, 
                'phone' => $this->phone,
                'address' => $this->address,
                'registration_date' => $this->registration_date

                );
            	if($this->db->insert('customer', $customer))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function noticeInsert()
        {
                 $this->type    = $this->input->post('type', TRUE);
                 $this->date  = $this->input->post('date', TRUE);
                 $this->description     = $this->input->post('description', TRUE);
              
            	$notice = array(
                'type' => $this->type,
                'date' => $this->date, 
                'description' =>  $this->description, 


                );
            	if($this->db->insert('noticeboard', $notice))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function categoryInsert()
        {
                 $this->type    = $this->input->post('type', TRUE);
              
            	$category = array(
                'type' => $this->type,


                );
            	if($this->db->insert('category', $category))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function expenseTypeInsert()
        {
                 $this->type    = $this->input->post('type', TRUE);
              
            	$expenseType = array(
                'type' => $this->type,


                );
            	if($this->db->insert('expense_type', $expenseType))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function expenseInsert()
        {
                 $this->ex_id    = $this->input->post('expense_id', TRUE);
                 $this->date    = $this->input->post('date', TRUE);
                 $this->amount    = $this->input->post('amount', TRUE);
              
            	$expense = array(
                'ex_id' => $this->ex_id,
                'date' => $this->date,
                'amount' => $this->amount,


                );
            	if($this->db->insert('expense', $expense))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function productInsert()
        {
                 $this->cat_id    = $this->input->post('category_id', TRUE);
                 $this->name    = $this->input->post('name', TRUE);
                 $this->date    = $this->input->post('date_publish', TRUE);
                 $this->description    = $this->input->post('description', TRUE);
                 $this->price    = $this->input->post('price', TRUE);
              
            	$product = array(
                'cat_id' => $this->cat_id,
                'name' => $this->name,
                'date_publish' => $this->date,
                'description' => $this->description,
                'price' => $this->price,


                );
            	if($this->db->insert('product', $product))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function orderInsert()
        {
                 $this->c_id    = $this->input->post('customer_id', TRUE);
                 $this->name    = $this->input->post('name', TRUE);
                 $this->description    = $this->input->post('description', TRUE);
                 $this->order_date    = $this->input->post('order_date', TRUE);
                 $this->delivery_date    = $this->input->post('delivery_date', TRUE);
                 $this->price    = $this->input->post('price', TRUE);
              
            	$order = array(
                'c_id' => $this->c_id,
                'name' => $this->name,
                'description' => $this->description,
                'order_date' => $this->order_date,
                'delivery_date' => $this->delivery_date,
                'price' => $this->price,


                );
            	if($this->db->insert('order_list', $order))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function orderChartInsert()
        {
                 $this->c_id    = $this->input->post('customer_id', TRUE);
                 $this->p_id    = $this->input->post('product_id', TRUE);
                 $this->date    = $this->input->post('date', TRUE);
                 
            	$orderChart = array(
                'c_id' => $this->c_id,
                'p_id' => $this->p_id,
                'date' => $this->date,
                );
				if($this->db->insert('order_chart', $orderChart))
                  
                   		return TRUE;
                   
                else
                    return FALSE;
            
        }
        public function salaryInsert()
        {
                 $this->e_id    = $this->input->post('employee_id', TRUE);
                 $this->salary    = $this->input->post('salary', TRUE);
              
                $salary = array(
                'e_id' => $this->e_id,
                'salary' => $this->salary,


                );
                if($this->db->insert('salary', $salary))
                    return TRUE;
                else
                    return FALSE;
            
        }
        public function salaryChartInsert()
        {
                 $this->e_id    = $this->input->post('employee_id', TRUE);
                 $this->date    = $this->input->post('date', TRUE);
                 $this->payment    = $this->input->post('payment', TRUE);
                 $this->withdraw    = $this->input->post('withdraw', TRUE);
                 $this->bonus    = $this->input->post('bonus', TRUE);
                 $this->increment    = $this->input->post('increment', TRUE);
                 $this->decrement    = $this->input->post('decrement', TRUE);
              
                $salaryChart = array(
                'e_id' => $this->e_id,
                'date' => $this->date,
                'payment' => $this->payment,
                'withdraw' => $this->withdraw,
                'bonus' => $this->bonus,
                'increment' => $this->increment,
                'decrement' => $this->decrement,


                );
                if($this->db->insert('salary_chart', $salaryChart))
                    return TRUE;
                else
                    return FALSE;
            
        }
 

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }
        public function userInsert()
        {
                 $this->username   = $this->input->post('username', TRUE);
                 $this->password    = $this->input->post('password', TRUE);
                 $this->user_access    = $this->input->post('user_access', TRUE);
                 
                $user = array(
                'username' => $this->username,
                'password' => $this->password,
                'user_access' => $this->user_access,
                );
                if($this->db->insert('users', $user))
                  
                        return TRUE;
                   
                else
                    return FALSE;
            
        }

}