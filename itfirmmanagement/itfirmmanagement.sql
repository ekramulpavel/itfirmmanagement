-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2015 at 07:05 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itfirmmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `type`) VALUES
(1, 'Windows'),
(2, 'IOS'),
(3, 'Desktop');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `c_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` varchar(255) NOT NULL,
  `registration_date` varchar(25) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `name`, `dob`, `email`, `phone`, `address`, `registration_date`, `image`) VALUES
(1, 'Shohanur Rahman', '04-06-1995', 'shohan.nsu.cse@gmail.com', '01959536850', '', '05-12-2015', NULL),
(2, 'Price Mahmud', '17-12-1984', 'prince@gmail.com', '01723461358', '', '05-12-2015', NULL),
(3, 'Shohan', '4-06-2015', 'shohanjh09@gmail.com', '0145545', 'Jessore', '04-10-1999', NULL),
(4, 'Rafiq', '04-06-2015', 'shohanjh09@gmail.com', '01723461358', 'Jessore', '04-10-1999', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `e_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `dob` varchar(25) NOT NULL,
  `hire_date` varchar(25) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`e_id`, `name`, `email`, `phone`, `dob`, `hire_date`, `address`, `image`) VALUES
(1, 'shohanur Rahman', 'shohan@gmail.com', '01959536850', '04-06-1995', '04-11-2011', 'Bashundhara R/A, Dhaka', NULL),
(2, 'Prince Mahmud', 'prince@gmail.com', '01723461358', '17-12-1984', '04-06-2010', 'Jessore', NULL),
(3, 'Ashiq', 'ashiq@gmail.com', '01918456565', '03-04-2015', '05-02-2015', 'Jessore', NULL),
(4, 'Ashiq', 'shohan.nsu.cse@gmail.com', '01723461358', '04-06-2015', '05-02-2015', 'Jessore', NULL),
(5, 'Showrov', 'showrov@gmail.com', '0179956566', '05-06-1988', '02-01-2014', 'Dhaka', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `ex_id` int(11) NOT NULL,
  `date` varchar(30) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `ex_id`, `date`, `amount`) VALUES
(1, 1, '04-06-2015', 100000),
(2, 2, '02-05-2015', 50000),
(3, 1, '04-12-2015', 1000),
(4, 3, '04-12-2015', 220);

-- --------------------------------------------------------

--
-- Table structure for table `expense_type`
--

CREATE TABLE `expense_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_type`
--

INSERT INTO `expense_type` (`id`, `type`) VALUES
(1, 'Salary'),
(2, 'Others'),
(3, 'Food');

-- --------------------------------------------------------

--
-- Table structure for table `noticeboard`
--

CREATE TABLE `noticeboard` (
  `n_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `date` varchar(30) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noticeboard`
--

INSERT INTO `noticeboard` (`n_id`, `type`, `date`, `description`) VALUES
(1, 'Discount', '04-06-2015', '50% discount is ready for the students only.'),
(2, 'Discount', '04-12-2015', '50% discount for NSU students.');

-- --------------------------------------------------------

--
-- Table structure for table `order_chart`
--

CREATE TABLE `order_chart` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `date` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_chart`
--

INSERT INTO `order_chart` (`id`, `c_id`, `p_id`, `date`) VALUES
(16, 2, 3, '08-12-2015');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `order_date` varchar(30) NOT NULL,
  `delivery_date` varchar(30) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `c_id`, `name`, `description`, `order_date`, `delivery_date`, `price`) VALUES
(1, 1, 'Prescription Pad', 'Only for the doctors.', '04-12-2015', '12-12-2015', 10000),
(2, 3, 'Rony', 'This is really nice one.', '04-12-2015', '01-02-2016', 56000);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `p_id` int(100) NOT NULL,
  `cat_id` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_publish` varchar(25) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`p_id`, `cat_id`, `name`, `date_publish`, `description`, `image`, `price`) VALUES
(3, 1, 'Shohan', '04-12-2015', 'This is really nice one.', '', 30000),
(4, 2, 'Prescription Pad', '01-02-2015', 'This is a automated software which is very helpful for the people of doctor.', '', 25000),
(5, 3, 'Ashiq', '01-02-2015', 'This is really nice one.', '', 30000),
(6, 2, 'Rafiq', '04-12-2015', 'This is a automated software which is very helpful for the people of doctor.', '', 30000),
(7, 1, 'Shohan', '04-12-2015', 'This is a automated software which is very helpful for the people of doctor.', '', 25000),
(9, 2, 'Ss', '01-02-2015', 'This is a automated software which is very helpful for the people of doctor.', '', 25000),
(10, 1, 'Meal Managment System', '04-12-2015', 'This is really nice one.', '', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `profit`
--

CREATE TABLE `profit` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `come_from` varchar(255) NOT NULL,
  `date` varchar(30) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profit`
--

INSERT INTO `profit` (`id`, `c_id`, `come_from`, `date`, `price`) VALUES
(5, 3, 'order_chart', '08-12-2015', 1),
(6, 4, 'order_chart', '08-12-2020', 1),
(7, 1, 'order_chart', '08-12-2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `e_id` int(11) NOT NULL,
  `salary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `e_id`, `salary`) VALUES
(1, 1, 10000),
(2, 2, 20000),
(3, 2, 30000);

-- --------------------------------------------------------

--
-- Table structure for table `salary_chart`
--

CREATE TABLE `salary_chart` (
  `id` int(11) NOT NULL,
  `e_id` int(11) NOT NULL,
  `date` varchar(30) NOT NULL,
  `payment` varchar(30) NOT NULL,
  `withdraw` varchar(30) NOT NULL,
  `bonus` int(11) NOT NULL,
  `increment` int(11) NOT NULL,
  `decrement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_chart`
--

INSERT INTO `salary_chart` (`id`, `e_id`, `date`, `payment`, `withdraw`, `bonus`, `increment`, `decrement`) VALUES
(1, 1, '04-06-2015', 'Accept', 'Paid', 2000, 500, 0),
(2, 2, '08-12-2015', 'accept', 'not Paid', 500, 200, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_access` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_access`) VALUES
(1, 'admin', 'admin', 'admin_page'),
(2, 'shohanjh09', 'shohanjh09', 'admin_page');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_type`
--
ALTER TABLE `expense_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticeboard`
--
ALTER TABLE `noticeboard`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `order_chart`
--
ALTER TABLE `order_chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `profit`
--
ALTER TABLE `profit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_chart`
--
ALTER TABLE `salary_chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `expense_type`
--
ALTER TABLE `expense_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `noticeboard`
--
ALTER TABLE `noticeboard`
  MODIFY `n_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_chart`
--
ALTER TABLE `order_chart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `p_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `profit`
--
ALTER TABLE `profit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `salary_chart`
--
ALTER TABLE `salary_chart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
